FROM gradle:latest as builder
COPY . /java-app
WORKDIR /java-app
RUN gradle bootJar

FROM alpine:latest
RUN apk add openjdk8
COPY --from=builder /java-app/build/libs/*.jar /java-app.jar
CMD java -jar /java-app.jar
