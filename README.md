# java-rest-application

Example application for demonstrating Springboot REST API application with AWS integrations

### Configurations:
* Create a copy from `local.env-temp` and rename it `local.env`
* Insert values to the file

### Build and run using docker-compose
`docker-compose up`

### Build application without Docker:
`./gradlew build`

### Run application without Docker:
`./gradlew bootRun`

### Run application without Docker and read envs from `local.env` file:
`./gradlew bootRun local`
