package org.joneteus.javarestapplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaRestApplication {
	private static final Logger logger = LoggerFactory.getLogger(JavaRestApplication.class);

	public static void main(String[] args) {
		locateAwsCredentialProfile();
		SpringApplication.run(JavaRestApplication.class, args);
	}

	private static void locateAwsCredentialProfile() {
		String awsProfileFromEnv = System.getenv("AWS_PROFILE");
		if(awsProfileFromEnv == null) {
			logger.warn("No AWS credentials profile found from environemnt variable");
		} else {
			logger.info("AWS credentials profile from environemnt variable: "+awsProfileFromEnv);
		}
	}
}
