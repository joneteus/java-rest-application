package org.joneteus.javarestapplication;

public class Question {

    private String name;

    public Question() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
