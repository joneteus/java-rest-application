package org.joneteus.javarestapplication;

import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/questions")
public class QuestionsController {

    private RestTemplate restTemplate;

    public QuestionsController() {
        this.restTemplate = new RestTemplate();
    }

    @GetMapping("")
    QuestionList getQuestions() {
        String stackQuestionsUrl = "https://api.github.com/search/repositories?q=language:java";

        QuestionList questionList = restTemplate.getForObject(stackQuestionsUrl, QuestionList.class);
        return questionList;
    }
}
