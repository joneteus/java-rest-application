package org.joneteus.javarestapplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/queue")
public class QueueController {
    private final Logger logger = LoggerFactory.getLogger(QueueController.class);
    private final SqsMessenger sqsMessenger = new SqsMessenger();


    @GetMapping("/receive")
    String receiveFromQueue() {
        return sqsMessenger.receiveFromSqs();
    }

    @PostMapping("/send")
    String sendToQueue(@RequestBody String msg) {
        return sqsMessenger.sendToSqs(msg);
    }

    @DeleteMapping("/purge")
    void purgeQueue() {
        sqsMessenger.purgeSqsQueue();
    }
}
