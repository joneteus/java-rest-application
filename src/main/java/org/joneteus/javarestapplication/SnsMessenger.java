package org.joneteus.javarestapplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.*;

import java.util.ArrayList;
import java.util.List;

class SnsMessenger {
    private final Logger logger = LoggerFactory.getLogger(SnsMessenger.class);

    // Read SNS topic ARN from environment variable
    private final String mySnsTopicArn= System.getenv("SNS_TOPIC_ARN");

    // Another way is to hard code the SNS topic ARN in code
    // private final String mySnsTopicArn = "arn:aws:sns:eu-west-1:054683117628:joneteus-test-topic";

    // AWS SDK will use the DefaultCredentialsProvider when no CredentialsProvider is given
    // It will look for credentials from default locations
    // https://docs.aws.amazon.com/sdk-for-java/v2/developer-guide/credentials.html
    private final SnsClient client = SnsClient.builder()
            .region(Region.EU_WEST_1)
            .build();

    List<String> ListSNSSubscriptions() {
        logger.debug("Retrieving the list of SNS topic subscrpitions");

        ListSubscriptionsByTopicRequest listReq = ListSubscriptionsByTopicRequest.builder()
                .topicArn(mySnsTopicArn)
                .build();

        ListSubscriptionsByTopicResponse listRes = client.listSubscriptionsByTopic(listReq);

        List<Subscription> subscriptions = listRes.subscriptions();

        // Iterate over the result list and get subscriptionArns into another list
        List<String> subscriptionArns = new ArrayList<>();
        for(Subscription sub: subscriptions) {
            String subscriptionArn = sub.subscriptionArn();
            subscriptionArns.add(subscriptionArn);
        }

        return subscriptionArns;
    }
}
