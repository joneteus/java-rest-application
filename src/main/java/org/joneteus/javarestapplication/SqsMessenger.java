package org.joneteus.javarestapplication;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sqs.SqsClient;
import software.amazon.awssdk.services.sqs.model.*;

class SqsMessenger {
    private final Logger logger = LoggerFactory.getLogger(SqsMessenger.class);

    // Read SQS queue url from environment variable
    private final String myQueueUrl = System.getenv("SQS_QUEUE_URL");

    // Another way is to hard code the SQS-queue url in code
    // private final String myQueueUrl = "https://sqs.eu-west-1.amazonaws.com/054683117628/joneteus-queue";

    // AWS SDK will use the DefaultCredentialsProvider when no CredentialsProvider is given
    // It will look for credentials from default locations
    // https://docs.aws.amazon.com/sdk-for-java/v2/developer-guide/credentials.html
    private final SqsClient client = SqsClient.builder()
            .region(Region.EU_WEST_1)
            .build();

    String sendToSqs(String message) {
        logger.debug("Sending message to SQS");

        SendMessageRequest msqReq = SendMessageRequest.builder()
                .queueUrl(myQueueUrl)
                .messageBody(message)
                .build();

        SendMessageResponse msgRes = client.sendMessage(msqReq);
        return msgRes.messageId();
    }

    String receiveFromSqs() {
        logger.debug("Receiving message from SQS");

        ReceiveMessageRequest msgReq = ReceiveMessageRequest.builder()
                .queueUrl(myQueueUrl)
                .maxNumberOfMessages(1)
                .build();


        ReceiveMessageResponse msgRes = client.receiveMessage(msgReq);

        if(msgRes.messages().isEmpty()) {
            return "[No messages in queue]";
        } else {
            Message sqsMessge = msgRes.messages().get(0);
            deleteMessageFromSqs(sqsMessge.receiptHandle());
            return sqsMessge.body();
        }
    }

    void purgeSqsQueue() {
        PurgeQueueRequest purgeReq = PurgeQueueRequest.builder()
                .queueUrl(myQueueUrl)
                .build();

        client.purgeQueue(purgeReq);
    }

    private void deleteMessageFromSqs(String sqsHandle) {
        DeleteMessageRequest delReq = DeleteMessageRequest.builder()
                .queueUrl(myQueueUrl)
                .receiptHandle(sqsHandle)
                .build();
        client.deleteMessage(delReq);
    }
}
