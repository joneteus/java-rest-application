package org.joneteus.javarestapplication;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/topic")
public class TopicController {

    private final SnsMessenger snsMessenger = new SnsMessenger();

    @GetMapping("/subscribers")
    List<String> listSubscribers() {
        return snsMessenger.ListSNSSubscriptions();
    }
}
